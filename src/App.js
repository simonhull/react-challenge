import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {MpLookupService} from "./services/mp-lookup-service";
import {MPSection} from "./components/mp-section";

class App extends Component {
    mpLookupService = MpLookupService;

    constructor(props) {
        super(props);
        this.state = {mp: {}, postalCode: '', formValue: '', formError: ''};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({formValue: event.target.value})
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({formError: '', mp: ''});
        const sanitizedValue = this.state.formValue.trim().toUpperCase().replace(/\s/g, '').toString();
        if (sanitizedValue === "") {
            this.setState({formError: "Please Enter A Postal Code!", formValue: ""});
        }
        this.mpLookupService.lookupMpByPostalCode(sanitizedValue).subscribe(result => {
            this.setState({mp: result});
        }, error => {
            this.setState({formError: "No results found for that postal code.  Please Try again."})
        });
    }

    render() {
        // Form Error Section
        let errorText;
        if (this.state.formError) {
            errorText = <div className="form-error">{this.state.formError}</div>;
        }

        // Main Section
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">React Tech Challenge</h1>
                </header>

                <div className="form-wrapper">
                    <div className="form">
                        <h4 className="App-intro">
                            Enter your postal code to find your MP!
                        </h4>
                        <form onSubmit={this.handleSubmit}>
                            <input value={this.state.formValue} onChange={this.handleChange} placeholder="Postal Code"/>
                            {errorText}
                            <br/>
                            <button id="submit-button" type="submit">Go</button>
                        </form>
                        <MPSection mp={this.state.mp}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
