import React, {Component} from 'react';

export class MPSection extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const mp = this.props.mp;
        if (mp.name) {
            return (
                <div id="mp-section">
                    <header>
                        <h2>Your MP is:</h2>
                    </header>
                    <div className="mp-details">
                        <h3 className={mp.party_name.toLowerCase()}>{`${mp.name} - ${mp.party_name} Party`}</h3>
                        <img className="mp-image" src={mp.photo_url} alt={`${mp.name}`}/>
                    </div>
                </div>
            )
        } else {
            return <div></div>
        }
    }
}
