import fetchJsonp from "fetch-jsonp";
import {from, of} from 'rxjs';
import {catchError, map, mergeMap, shareReplay} from "rxjs/operators";


export class MpLookupService {

    /**
     * Returns the MP for a specific riding based upon the postal code provided.
     * @param {string} postalCode
     * @returns {Observable}
     */

    static lookupMpByPostalCode(postalCode) {
        return from(fetchJsonp(`https://represent.opennorth.ca/postcodes/${postalCode}`)).pipe(
            // Catch any errors due to incorrect postal codes.
            catchError((error) => {
                return of([])
            }),
            // Wait for the result then parse JSON.
            mergeMap(result => result.json()),
            // Take the resulting JSON and grab only the Representatives.
            map(data => data.representatives_centroid),
            // Take the Representatives and pick off only the MP
            map((candidates) => candidates.filter(candidate => candidate.elected_office === 'MP')),
            // Since we still have an array after filtering, reduce to object.
            map(candidateArray => candidateArray[0]),
            // Cache the value in case anything else needs to subscribe to it.
            shareReplay(),
        )
    }


}
